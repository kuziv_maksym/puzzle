package main.game;

import main.buttons.MyButton;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.awt.image.BufferedImage;

public class Game extends JFrame {

    private JPanel panel;
    private BufferedImage sourceImgae;
    private BufferedImage resizedImage;
    private Image image;
    private MyButton emptyButton;
    private int width;
    private int height;

    private List<MyButton> buttons;
    private List<Point> solution;
    private List<Point> current;

    private final int STAND_SIZE = 500;


    public Game() throws IOException {
        init();
    }

    private void init() throws IOException {
        initPoints();
        initJpanel();
        setImageResize();
        add(panel, BorderLayout.CENTER);
        dropButtons();
        Collections.shuffle(buttons);
        buttons.add(emptyButton);
        action();
        pack();
        setTitle("Puzzle");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void initPoints() {
        solution = new ArrayList<>();
        solution.add(new Point(0, 0));
        solution.add(new Point(0, 1));
        solution.add(new Point(0, 2));

        solution.add(new Point(1, 0));
        solution.add(new Point(1, 1));
        solution.add(new Point(1, 2));

        solution.add(new Point(2, 0));
        solution.add(new Point(2, 1));
        solution.add(new Point(2, 2));

        solution.add(new Point(3, 0));
        solution.add(new Point(3, 1));
        solution.add(new Point(3, 2));

    }


    private void initJpanel() {
        buttons = new ArrayList<>();
        panel = new JPanel();
        panel.setBorder(BorderFactory.createLineBorder(Color.gray));
        panel.setLayout(new GridLayout(4, 3, 0, 0));
    }

    private BufferedImage readImage(String path) throws IOException {
        File file = new File(path);
        var bimg = ImageIO.read(file);
        return bimg;
    }

    private int getNewHeight(int w, int h) {
        double ratio = STAND_SIZE / (double) w;
        int newHeight = (int) (h * ratio);
        return newHeight;
    }

    private BufferedImage resizeImage(BufferedImage originalImage, int width,
                                      int height, int type) {
        BufferedImage bufferedImage = new BufferedImage(width, height, type);
        Graphics2D g = bufferedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, width, height, null);
        g.dispose();
        return bufferedImage;
    }

    private void setImageResize() throws IOException {
        sourceImgae = readImage("image.jpg");
        int new_height = getNewHeight(sourceImgae.getWidth(), sourceImgae.getHeight());
        resizedImage = resizeImage(sourceImgae, STAND_SIZE, new_height,
                BufferedImage.TYPE_INT_RGB);

        width = resizedImage.getWidth();
        height = resizedImage.getHeight();
    }

    private void dropButtons() {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 3; j++) {
                image = createImage(new FilteredImageSource(resizedImage.getSource(),
                        new CropImageFilter(j * width / 3, i * height / 4,
                                (width / 3), height / 4)));

                MyButton button = new MyButton(image);
                button.putClientProperty("position", new Point(i, j));
                if (i == 3 && j == 2) {

                    emptyButton = new MyButton();
                    emptyButton.setBorderPainted(false);
                    emptyButton.setContentAreaFilled(false);
                    emptyButton.setEmptyButton();
                    emptyButton.putClientProperty("position", new Point(i, j));
                } else {
                    buttons.add(button);
                }
            }
        }
    }

    private void action() {
        for (int i = 0; i < 12; i++) {
            JButton btn = buttons.get(i);
            panel.add(btn);
            btn.setBorder(BorderFactory.createLineBorder(Color.black));
            btn.addActionListener(new ClickAction());
        }
    }


    private class ClickAction extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            checkButton(e);
        }

        private void checkButton(ActionEvent e) {

            int clickIndex = 0;

            for (MyButton button : buttons) {
                if (button.isEmptyButton()) {
                    clickIndex = buttons.indexOf(button);
                }
            }

            JButton button = (JButton) e.getSource();
            int emptyIndex = buttons.indexOf(button);

            if ((emptyIndex - 1 == clickIndex) || (emptyIndex + 1 == clickIndex) ||
                    (emptyIndex - 2 == clickIndex) || (emptyIndex + 2 == clickIndex) ||
                    (emptyIndex - 3 == clickIndex) || (emptyIndex + 3 == clickIndex) ||
                    (emptyIndex - 4 == clickIndex) || (emptyIndex + 4 == clickIndex) ||
                    (emptyIndex - 5 == clickIndex) || (emptyIndex + 5 == clickIndex) ||
                    (emptyIndex - 6 == clickIndex) || (emptyIndex + 6 == clickIndex) ||
                    (emptyIndex - 7 == clickIndex) || (emptyIndex + 7 == clickIndex) ||
                    (emptyIndex - 8 == clickIndex) || (emptyIndex + 8 == clickIndex) ||
                    (emptyIndex - 9 == clickIndex) || (emptyIndex + 9 == clickIndex) ||
                    (emptyIndex - 10 == clickIndex) || (emptyIndex + 10 == clickIndex) ||
                    (emptyIndex - 11 == clickIndex) || (emptyIndex + 11 == clickIndex) ||
                    (emptyIndex - 12 == clickIndex) || (emptyIndex + 12 == clickIndex)) {
                Collections.swap(buttons, emptyIndex, clickIndex);
                refreshButtons();
                checkEnd();
            }
            if (emptyIndex == clickIndex) {
                    while (true) {
                        autoCreate();
                        if (checkEnd()) {
                            break;
                        }
                    }
            }
        }

        boolean checkEnd() {
            if (compareList(solution, current)) {
                JOptionPane.showMessageDialog(panel, "Finished",
                        "Congratulation", JOptionPane.INFORMATION_MESSAGE);
                return true;

            }
            return false;
        }


        private void refreshButtons() {
            panel.removeAll();
            for (JComponent btn : buttons) {
                panel.add(btn);
            }
            panel.validate();
        }

        private void autoCreate() {
            current = new ArrayList<>();
            for (JComponent btn : buttons) {
                current.add((Point) btn.getClientProperty("position"));
            }
            int i = 0;
            int j = 0;
            while (true) {
                if (j == 11) {
                    break;
                }
                if (solution.get(i).equals(current.get(j))) {
                    i++;
                } else {
                    if (j == 11) {
                        Collections.swap(current, i,j);
                        Collections.swap(buttons, i, j);
                        refreshButtons();
                        i++;
                        j=i;
                    } else {
                        j++;
                        Collections.swap(buttons, i, j);
                        refreshButtons();
                    }
                }
            }
        }
    }

    public static boolean compareList(List ls1, List ls2) {

        return ls1.toString().contentEquals(ls2.toString());
    }
}
