package main.buttons;

import javax.swing.*;
import java.awt.*;

public class MyButton extends JButton {

    private boolean emptyButton;

    public MyButton() {
        super();
        init();
    }

    public MyButton(Image image) {
        super(new ImageIcon(image));
        init();
    }

    private void init() {
        emptyButton = false;
        BorderFactory.createLineBorder(Color.gray);
    }

    public void setEmptyButton() {
        emptyButton = true;
    }

    public boolean isEmptyButton() {
        return emptyButton;
    }
}