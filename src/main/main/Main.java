package main.main;

import main.game.Game;

import java.awt.*;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {

            Game Game = null;
            try {
                Game = new Game();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Game.setVisible(true);
        });
}}
